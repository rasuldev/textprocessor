﻿import React, { Component } from 'react';

export class TextProcessor extends Component {
	static displayName = TextProcessor.name;

	constructor(props) {
		super(props);
		this.state = { value: '', choice: 'ConvertToDollars' };

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.currentRequest = this.currentRequest.bind(this);
	}

	handleChange(event) {
		this.setState({ value: event.target.value });
	}

	handleSubmit(event) {
		alert('Operation selected: ' + this.state.value);
		event.preventDefault();
	}

	currentRequest(event) {
		this.setState({ choice: event.target.value });
    }

	render() {
		return (
			<form onSubmit={this.handleSubmit}>
				<fieldset>
					<legend>Enter value</legend>
					<textarea value={this.state.value} onChange={this.handleChange} />
					<select value={this.state.choice} onChange={this.currentRequest}>
						<option value="ConvertToDollars">ConvertToDollars</option>
						<option value="MakeEquationWithLabelsFromDollars">MakeEquationWithLabelsFromDollars</option>
						<option value="WrapInEnvironment">WrapInEnvironment</option>
						<option value="MergeBibitemsAndReplaceCites">MergeBibitemsAndReplaceCites</option>
						<option value="ArrangeCites">ArrangeCites</option>
						<option value="ArrangeCitesAndRenameToNumbers">ArrangeCitesAndRenameToNumbers</option>
						<option value="ConvertRBibitemsToBibitems">ConvertRBibitemsToBibitems</option>
					</select>
					<input type="submit" value="Send message" />
				</fieldset>

				<fieldset>
					<legend>Output value</legend>
					<p>{this.state.value}</p>
				</fieldset>
			</form>
		);
	}
}
