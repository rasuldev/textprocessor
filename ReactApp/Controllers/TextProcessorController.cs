﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TextProcessor;

namespace ReactApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TextProcessorController : Controller
    {
        /// <summary>
        /// MakeDollarsFromEquationWithLabels
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult ConvertToDollars(string text)
        {
            try
            {
                var result = CommonProcessor.MakeDollarsFromEquationWithLabels(text);
                return Ok(result);
            }
            catch (Exception)
            {
                return this.StatusCode((int)HttpStatusCode.BadRequest);
            }
        }
        [HttpPost]
        public IActionResult MakeEquationWithLabelsFromDollars(string text)
        {
            try
            {
                var result = CommonProcessor.MakeEquationWithLabelsFromDollars(text);
                return Ok(result);
            }
            catch (Exception)
            {
                return this.StatusCode((int)HttpStatusCode.BadRequest);
            }
        }
        [HttpPost]
        public IActionResult WrapInEnvironment(string source, string startBlock, string endBlock, string blocknameInText, string envName, Func<string, string> labelForNum)
        {
            try
            {
                var result = CommonProcessor.WrapInEnvironment(source,  startBlock,  endBlock,  blocknameInText,  envName, labelForNum);
                return Ok(result);
            }
            catch (Exception)
            {
                return this.StatusCode((int)HttpStatusCode.BadRequest);
            }
        }
        [HttpPost]
        public IActionResult MergeBibitemsAndReplaceCites(IList<string> sources)
        {
            try
            {
                var result = CommonProcessor.MergeBibitemsAndReplaceCites(sources);
                return Ok(result);
            }
            catch (Exception e)
            {
                return this.StatusCode((int)HttpStatusCode.BadRequest);
            }
        }
        [HttpPost]
        public IActionResult ArrangeCites(string text)
        {
            try
            {
                var result = CommonProcessor.ArrangeCites(text);
                return Ok(result);
            }
            catch (Exception)
            {
                return this.StatusCode((int)HttpStatusCode.BadRequest);
            }
        }
        [HttpPost]
        public IActionResult ArrangeCitesAndRenameToNumbers(string text)
        {
            try
            {
                var result = CommonProcessor.ArrangeCitesAndRenameToNumbers(text);
                return Ok(result);
            }
            catch (Exception)
            {
                return this.StatusCode((int)HttpStatusCode.BadRequest);
            }
        }
         [HttpPost]
        public IActionResult ConvertRBibitemsToBibitems(string text)
        {
            try
            {
                var result = CommonProcessor.ConvertRBibitemsToBibitems(text);
                return Ok(result);
            }
            catch (Exception)
            {
                return this.StatusCode((int)HttpStatusCode.BadRequest);
            }
        }
    }
}
